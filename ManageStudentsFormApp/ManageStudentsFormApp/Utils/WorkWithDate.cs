﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Utils
{
    class WorkWithDate
    {
        public static int WeekByDate(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
                time = time.AddDays(3);

            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        public static int CurrentWeek()
        {
            DateTime time = DateTime.Now;
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
                time = time.AddDays(3);
            
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        public static DateTime currentDate = DateTime.Now;
        private static DateTime startDate = new DateTime(2018, 10, 1);
        private static DateTime startRestDate = new DateTime(2018, 12, 22);
        private static DateTime endRestDate = new DateTime(2019, 1, 6);
        private static DateTime endDate = new DateTime(2019, 1, 19);
        private static DateTime endYear = new DateTime(2018, 12, 31);
        private static DateTime startNewYear = new DateTime(2019, 1, 1);
        public static int GetCurrentWeek(DateTime date)
        {
            if (date.CompareTo(startDate) < 0 || date.CompareTo(endDate) > 0)
                throw new Exception("This date is not in the current academic year");
            if ((date.CompareTo(startRestDate) > 0 && date.CompareTo(endYear) < 0) || (date.CompareTo(startNewYear) > 0) && (date.CompareTo(endRestDate)) < 0)
                throw new Exception("This date seems to be during the holiday!");
            if (date.CompareTo(startRestDate) > 0 && date.CompareTo(endYear) < 0)
                return 12;
            if (date.CompareTo(endRestDate) > 0)
                return 12 + (WeekByDate(date) - WeekByDate(endRestDate));
            if (date.CompareTo(startRestDate) < 0)
                return WeekByDate(date) - WeekByDate(startDate) + 1;
            throw new Exception("Wrong date");

        }
    }
}
