﻿namespace ManageStudentsFormApp
{
    partial class GradeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataViewGrade = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.CheckBoxMotivat = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxGrade = new System.Windows.Forms.TextBox();
            this.TextBoxTeacher = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ComboBoxStudent = new System.Windows.Forms.ComboBox();
            this.ComboBoxTask = new System.Windows.Forms.ComboBox();
            this.AddBtnGrade = new System.Windows.Forms.Button();
            this.ClearBtnGrade = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.CBoxTaskFiltr = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.CBoxStudentFiltr = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.CBoxGroupGrFiltr = new System.Windows.Forms.ComboBox();
            this.CBoxTaskGrFiltr = new System.Windows.Forms.ComboBox();
            this.ButtonListGr = new System.Windows.Forms.Button();
            this.DatePicketFrom = new System.Windows.Forms.DateTimePicker();
            this.DatePicketTo = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.ButtonListDate = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.BtnStudentsOnTime = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnAcceptedStudentInExam = new System.Windows.Forms.Button();
            this.BtnHardestTask = new System.Windows.Forms.Button();
            this.BtnTaskGradeEachStudent = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataViewGrade)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataViewGrade
            // 
            this.DataViewGrade.AllowUserToAddRows = false;
            this.DataViewGrade.AllowUserToDeleteRows = false;
            this.DataViewGrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataViewGrade.Location = new System.Drawing.Point(12, 243);
            this.DataViewGrade.Name = "DataViewGrade";
            this.DataViewGrade.ReadOnly = true;
            this.DataViewGrade.RowTemplate.Height = 24;
            this.DataViewGrade.Size = new System.Drawing.Size(589, 320);
            this.DataViewGrade.TabIndex = 0;
            this.DataViewGrade.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataViewGrade_CellContentClick);
            this.DataViewGrade.CurrentCellChanged += new System.EventHandler(this.DataViewGrade_CellFormatting);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(760, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Grade Informations";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.10945F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.89055F));
            this.tableLayoutPanel1.Controls.Add(this.CheckBoxMotivat, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.TextBoxGrade, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TextBoxTeacher, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ComboBoxStudent, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ComboBoxTask, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(649, 61);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(402, 242);
            this.tableLayoutPanel1.TabIndex = 2;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // CheckBoxMotivat
            // 
            this.CheckBoxMotivat.AutoSize = true;
            this.CheckBoxMotivat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBoxMotivat.Location = new System.Drawing.Point(115, 195);
            this.CheckBoxMotivat.Name = "CheckBoxMotivat";
            this.CheckBoxMotivat.Size = new System.Drawing.Size(18, 17);
            this.CheckBoxMotivat.TabIndex = 3;
            this.CheckBoxMotivat.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Grade";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Task";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 25);
            this.label5.TabIndex = 3;
            this.label5.Text = "Teacher";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 25);
            this.label6.TabIndex = 4;
            this.label6.Text = "Motivat";
            // 
            // TextBoxGrade
            // 
            this.TextBoxGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxGrade.Location = new System.Drawing.Point(115, 51);
            this.TextBoxGrade.Name = "TextBoxGrade";
            this.TextBoxGrade.Size = new System.Drawing.Size(149, 30);
            this.TextBoxGrade.TabIndex = 5;
            // 
            // TextBoxTeacher
            // 
            this.TextBoxTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxTeacher.Location = new System.Drawing.Point(115, 147);
            this.TextBoxTeacher.Name = "TextBoxTeacher";
            this.TextBoxTeacher.Size = new System.Drawing.Size(149, 30);
            this.TextBoxTeacher.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 25);
            this.label7.TabIndex = 9;
            this.label7.Text = "Student";
            // 
            // ComboBoxStudent
            // 
            this.ComboBoxStudent.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComboBoxStudent.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ComboBoxStudent.FormattingEnabled = true;
            this.ComboBoxStudent.Location = new System.Drawing.Point(115, 3);
            this.ComboBoxStudent.Name = "ComboBoxStudent";
            this.ComboBoxStudent.Size = new System.Drawing.Size(149, 24);
            this.ComboBoxStudent.TabIndex = 10;
            this.ComboBoxStudent.SelectedIndexChanged += new System.EventHandler(this.ComboBoxStudent_SelectedIndexChanged);
            this.ComboBoxStudent.TextChanged += new System.EventHandler(this.ComboBoxStudent_TextChanged);
            // 
            // ComboBoxTask
            // 
            this.ComboBoxTask.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComboBoxTask.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ComboBoxTask.FormattingEnabled = true;
            this.ComboBoxTask.Location = new System.Drawing.Point(115, 99);
            this.ComboBoxTask.Name = "ComboBoxTask";
            this.ComboBoxTask.Size = new System.Drawing.Size(149, 24);
            this.ComboBoxTask.TabIndex = 11;
            this.ComboBoxTask.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTask_SelectedIndexChanged);
            // 
            // AddBtnGrade
            // 
            this.AddBtnGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtnGrade.Location = new System.Drawing.Point(657, 309);
            this.AddBtnGrade.Name = "AddBtnGrade";
            this.AddBtnGrade.Size = new System.Drawing.Size(75, 40);
            this.AddBtnGrade.TabIndex = 3;
            this.AddBtnGrade.Text = "Add";
            this.AddBtnGrade.UseVisualStyleBackColor = true;
            this.AddBtnGrade.Click += new System.EventHandler(this.AddBtnGrade_Click);
            // 
            // ClearBtnGrade
            // 
            this.ClearBtnGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearBtnGrade.Location = new System.Drawing.Point(764, 309);
            this.ClearBtnGrade.Name = "ClearBtnGrade";
            this.ClearBtnGrade.Size = new System.Drawing.Size(75, 39);
            this.ClearBtnGrade.TabIndex = 4;
            this.ClearBtnGrade.Text = "Clear";
            this.ClearBtnGrade.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "Filters";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // CBoxTaskFiltr
            // 
            this.CBoxTaskFiltr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBoxTaskFiltr.FormattingEnabled = true;
            this.CBoxTaskFiltr.Location = new System.Drawing.Point(175, 61);
            this.CBoxTaskFiltr.Name = "CBoxTaskFiltr";
            this.CBoxTaskFiltr.Size = new System.Drawing.Size(121, 28);
            this.CBoxTaskFiltr.TabIndex = 6;
            this.CBoxTaskFiltr.SelectedIndexChanged += new System.EventHandler(this.CBoxTaskFiltr_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(14, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Notele la o tema:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(14, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(158, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Notele unui student:";
            // 
            // CBoxStudentFiltr
            // 
            this.CBoxStudentFiltr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBoxStudentFiltr.FormattingEnabled = true;
            this.CBoxStudentFiltr.Location = new System.Drawing.Point(197, 105);
            this.CBoxStudentFiltr.Name = "CBoxStudentFiltr";
            this.CBoxStudentFiltr.Size = new System.Drawing.Size(121, 28);
            this.CBoxStudentFiltr.TabIndex = 9;
            this.CBoxStudentFiltr.SelectedIndexChanged += new System.EventHandler(this.CBoxStudentFiltr_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 20);
            this.label10.TabIndex = 10;
            this.label10.Text = "Notele la o grupa:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(294, 155);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 20);
            this.label11.TabIndex = 11;
            this.label11.Text = "si o tema:";
            // 
            // CBoxGroupGrFiltr
            // 
            this.CBoxGroupGrFiltr.FormattingEnabled = true;
            this.CBoxGroupGrFiltr.Location = new System.Drawing.Point(167, 151);
            this.CBoxGroupGrFiltr.Name = "CBoxGroupGrFiltr";
            this.CBoxGroupGrFiltr.Size = new System.Drawing.Size(121, 24);
            this.CBoxGroupGrFiltr.TabIndex = 12;
            this.CBoxGroupGrFiltr.SelectedIndexChanged += new System.EventHandler(this.CBoxGroupGrFiltr_SelectedIndexChanged);
            // 
            // CBoxTaskGrFiltr
            // 
            this.CBoxTaskGrFiltr.FormattingEnabled = true;
            this.CBoxTaskGrFiltr.Location = new System.Drawing.Point(383, 155);
            this.CBoxTaskGrFiltr.Name = "CBoxTaskGrFiltr";
            this.CBoxTaskGrFiltr.Size = new System.Drawing.Size(121, 24);
            this.CBoxTaskGrFiltr.TabIndex = 13;
            this.CBoxTaskGrFiltr.SelectedIndexChanged += new System.EventHandler(this.CBoxTaskGrFiltr_SelectedIndexChanged);
            // 
            // ButtonListGr
            // 
            this.ButtonListGr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonListGr.Location = new System.Drawing.Point(526, 150);
            this.ButtonListGr.Name = "ButtonListGr";
            this.ButtonListGr.Size = new System.Drawing.Size(75, 29);
            this.ButtonListGr.TabIndex = 14;
            this.ButtonListGr.Text = "Afiseaza";
            this.ButtonListGr.UseVisualStyleBackColor = true;
            this.ButtonListGr.Click += new System.EventHandler(this.ButtonListGr_Click);
            // 
            // DatePicketFrom
            // 
            this.DatePicketFrom.Location = new System.Drawing.Point(129, 190);
            this.DatePicketFrom.Name = "DatePicketFrom";
            this.DatePicketFrom.Size = new System.Drawing.Size(170, 22);
            this.DatePicketFrom.TabIndex = 15;
            this.DatePicketFrom.ValueChanged += new System.EventHandler(this.DatePicketFrom_ValueChanged);
            // 
            // DatePicketTo
            // 
            this.DatePicketTo.Location = new System.Drawing.Point(343, 192);
            this.DatePicketTo.Name = "DatePicketTo";
            this.DatePicketTo.Size = new System.Drawing.Size(161, 22);
            this.DatePicketTo.TabIndex = 16;
            this.DatePicketTo.ValueChanged += new System.EventHandler(this.DatePicketTo_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(13, 190);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 20);
            this.label12.TabIndex = 17;
            this.label12.Text = "Notele intre:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(305, 192);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 20);
            this.label13.TabIndex = 18;
            this.label13.Text = "si :";
            // 
            // ButtonListDate
            // 
            this.ButtonListDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonListDate.Location = new System.Drawing.Point(526, 192);
            this.ButtonListDate.Name = "ButtonListDate";
            this.ButtonListDate.Size = new System.Drawing.Size(75, 30);
            this.ButtonListDate.TabIndex = 19;
            this.ButtonListDate.Text = "Afiseaza";
            this.ButtonListDate.UseVisualStyleBackColor = true;
            this.ButtonListDate.Click += new System.EventHandler(this.ButtonListDate_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(526, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 27);
            this.button1.TabIndex = 20;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnStudentsOnTime
            // 
            this.BtnStudentsOnTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStudentsOnTime.Location = new System.Drawing.Point(707, 404);
            this.BtnStudentsOnTime.Name = "BtnStudentsOnTime";
            this.BtnStudentsOnTime.Size = new System.Drawing.Size(318, 29);
            this.BtnStudentsOnTime.TabIndex = 21;
            this.BtnStudentsOnTime.Text = "Studentii care au predat la timp";
            this.BtnStudentsOnTime.UseVisualStyleBackColor = true;
            this.BtnStudentsOnTime.Click += new System.EventHandler(this.BtnStudentsOnTime_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(822, 376);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 25);
            this.label14.TabIndex = 22;
            this.label14.Text = "Rapoarte";
            // 
            // BtnAcceptedStudentInExam
            // 
            this.BtnAcceptedStudentInExam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcceptedStudentInExam.Location = new System.Drawing.Point(707, 439);
            this.BtnAcceptedStudentInExam.Name = "BtnAcceptedStudentInExam";
            this.BtnAcceptedStudentInExam.Size = new System.Drawing.Size(318, 34);
            this.BtnAcceptedStudentInExam.TabIndex = 23;
            this.BtnAcceptedStudentInExam.Text = "Studentii care pot intra in examen";
            this.BtnAcceptedStudentInExam.UseVisualStyleBackColor = true;
            this.BtnAcceptedStudentInExam.Click += new System.EventHandler(this.BtnAcceptedStudentInExam_Click);
            // 
            // BtnHardestTask
            // 
            this.BtnHardestTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnHardestTask.Location = new System.Drawing.Point(707, 479);
            this.BtnHardestTask.Name = "BtnHardestTask";
            this.BtnHardestTask.Size = new System.Drawing.Size(318, 46);
            this.BtnHardestTask.TabIndex = 24;
            this.BtnHardestTask.Text = "Cea mai grea tema";
            this.BtnHardestTask.UseVisualStyleBackColor = true;
            this.BtnHardestTask.Click += new System.EventHandler(this.BtnHardestTask_Click);
            // 
            // BtnTaskGradeEachStudent
            // 
            this.BtnTaskGradeEachStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTaskGradeEachStudent.Location = new System.Drawing.Point(707, 531);
            this.BtnTaskGradeEachStudent.Name = "BtnTaskGradeEachStudent";
            this.BtnTaskGradeEachStudent.Size = new System.Drawing.Size(318, 32);
            this.BtnTaskGradeEachStudent.TabIndex = 25;
            this.BtnTaskGradeEachStudent.Text = "Nota la laborator pentru fiecare student";
            this.BtnTaskGradeEachStudent.UseVisualStyleBackColor = true;
            this.BtnTaskGradeEachStudent.Click += new System.EventHandler(this.BtnTaskGradeEachStudent_Click);
            // 
            // GradeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1157, 582);
            this.Controls.Add(this.BtnTaskGradeEachStudent);
            this.Controls.Add(this.BtnHardestTask);
            this.Controls.Add(this.BtnAcceptedStudentInExam);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.BtnStudentsOnTime);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ButtonListDate);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.DatePicketTo);
            this.Controls.Add(this.DatePicketFrom);
            this.Controls.Add(this.ButtonListGr);
            this.Controls.Add(this.CBoxTaskGrFiltr);
            this.Controls.Add(this.CBoxGroupGrFiltr);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.CBoxStudentFiltr);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.CBoxTaskFiltr);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ClearBtnGrade);
            this.Controls.Add(this.AddBtnGrade);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DataViewGrade);
            this.Name = "GradeView";
            this.Text = "GradeView";
            this.Load += new System.EventHandler(this.GradeView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataViewGrade)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataViewGrade;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox CheckBoxMotivat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBoxGrade;
        private System.Windows.Forms.TextBox TextBoxTeacher;
        private System.Windows.Forms.Button AddBtnGrade;
        private System.Windows.Forms.Button ClearBtnGrade;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ComboBoxStudent;
        private System.Windows.Forms.ComboBox ComboBoxTask;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CBoxTaskFiltr;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CBoxStudentFiltr;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CBoxGroupGrFiltr;
        private System.Windows.Forms.ComboBox CBoxTaskGrFiltr;
        private System.Windows.Forms.Button ButtonListGr;
        private System.Windows.Forms.DateTimePicker DatePicketFrom;
        private System.Windows.Forms.DateTimePicker DatePicketTo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button ButtonListDate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BtnStudentsOnTime;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button BtnAcceptedStudentInExam;
        private System.Windows.Forms.Button BtnHardestTask;
        private System.Windows.Forms.Button BtnTaskGradeEachStudent;
    }
}