﻿using ManageStudentsFormApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManageStudentsFormApp
{
    partial class MainMenu : Form
    {
        public StudentService _studentService;
        public TaskService _taskService;
        public GradeService _gradeService;
        public MainMenu(StudentService studentService, TaskService taskService,GradeService gradeService)
        {
            _studentService = studentService;
            _taskService = taskService;
            _gradeService = gradeService;
            InitializeComponent();
        }

        private void StudentBtnStart_Click(object sender, EventArgs e)
        {
            // this.Hide();
            StudentView studentView = new StudentView(_studentService);
            studentView.Show();
        }

        private void TaskBtnStart_Click(object sender, EventArgs e)
        {
            TaskView taskView = new TaskView(_taskService,_studentService);
            taskView.Show();
        }

        private void GradeBtnStart_Click(object sender, EventArgs e)
        {
            GradeView gradeView = new GradeView(_studentService,_taskService,_gradeService);
            gradeView.Show();
        }
    }
}
