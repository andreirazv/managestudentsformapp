﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Repository;
using ManageStudentsFormApp.Utils;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using static ManageStudentsFormApp.Utils.WorkWithDate;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Services
{
    class GradeService : Service
    {
        public GradeService(ICrudRepository<ObjectId, Student> studentRepo, ICrudRepository<ObjectId, Grade> gradeRepo, ICrudRepository<ObjectId, Domain.Task> taskRepo) : base(studentRepo, gradeRepo, taskRepo) { }
        public bool SaveGrade(Grade grade,bool motivat)
        {
            double nota = grade.Nota;
            Domain.Task task = GetTask(ObjectId.Parse(grade.TaskId));
            if (GetAll().ToList().Find(x => x.TaskId.ToString().Equals(task._id.ToString())) != null)
                throw new Exception("Pentru aceasta tema a fost data o nota!");
            Student student = GetStudent(ObjectId.Parse(task.StudentId));
            DateTime deadLine = task.DeadLine;
            double extraPoints = student.ExtraPoints;
            int week = GetCurrentWeek(grade.Date) - GetCurrentWeek(deadLine);
            if (week > 2&&!motivat)
                nota = 1;
            else
            {
                if ((week <= 2 )||( motivat))
                {
                    if (week <= 0)
                        week = 0;
                    if(!motivat)
                        nota = nota - (2.5 * week);

                    nota = nota + extraPoints;
                    if (nota <= 10)
                        extraPoints = 0;
                    else
                    {
                        extraPoints = nota - 10;
                        nota = 10;
                    }
                }
            }
            grade.Nota = nota;
            if (_gradeRepository.Save(grade))
            {
                student.ExtraPoints = extraPoints;
               return _studentRepository.Update(student);
            }
            return false;
        }
        public IList<Grade> GetAll()
        {
            return _gradeRepository.GetAll().ToList();
        }
        public IList<Grade> GetByDates(DateTime from,DateTime to)
        {
            IEnumerable<Grade> grades =
            from grade in GetAll()
            where grade.Date.CompareTo(@from) > 0 && grade.Date.CompareTo(@to) < 0
            select grade;
            return grades.ToList();
        }
        public IList<Grade> GetByStudent(string student)
        {
            IEnumerable<Grade> grades =
            from grade in GetAll()
            where GetStudent(ObjectId.Parse(GetTask(ObjectId.Parse(grade.TaskId)).StudentId)).Name.Equals(@student)
            select grade;
            return grades.ToList();
        }
        public IList<Grade> GetByGroupAndTask(string groupStr,string task)
        {
            string taskWeek = task.Split(' ')[1];
            IEnumerable<Grade> grades =
            from grade in GetAll()
            where GetStudent(ObjectId.Parse(GetTask(ObjectId.Parse(grade.TaskId)).StudentId)).Group.ToString().Equals(@groupStr)
            && GetCurrentWeek(GetTask(ObjectId.Parse(grade.TaskId)).DeadLine).ToString().Equals(taskWeek)
            select grade;
            return grades.ToList();
        }
        public IList<Grade> GetByTask(string task)
        {
            string taskWeek = task.Split(' ')[1];
            IEnumerable<Grade> grades =
            from grade in GetAll()
            where GetCurrentWeek(GetTask(ObjectId.Parse(grade.TaskId)).DeadLine).ToString().Equals(taskWeek)
            select grade;
            return grades.ToList();
        }
    }
}
