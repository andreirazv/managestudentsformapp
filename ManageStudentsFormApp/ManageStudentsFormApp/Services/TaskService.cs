﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Repository;
using ManageStudentsFormApp.Utils;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using static ManageStudentsFormApp.Utils.WorkWithDate;
using Task = ManageStudentsFormApp.Domain.Task;

namespace ManageStudentsFormApp.Services
{
    class TaskService : Service
    {
        public TaskService(ICrudRepository<ObjectId, Student> studentRepo, ICrudRepository<ObjectId, Grade> gradeRepo, ICrudRepository<ObjectId, Task> taskRepo) : base(studentRepo, gradeRepo, taskRepo) { }
        public bool SaveTask(Task task)
        {
            return _taskRepository.Save(task);
        }
        public IList<Task> GetAll()
        {
            return _taskRepository.GetAll().ToList();
        }
        public void ExtendDeadline(ObjectId id ,int week)
        {
            Task task = GetTask(id);

            if ((CurrentWeek()-WeekByDate( task.DeadLine)) < 0)
                throw new ArgumentException("Data Curent este mai mare decat data pt deadline");
            task.DeadLine = task.DeadLine.AddDays(week*7);
            _taskRepository.Update(task);
        }
        public string GetTaskByWeekAndStudent(string student, string week)
        {
            return GetAll().ToList().Find(s => GetStudent(ObjectId.Parse(s.StudentId)).Name.Equals(student) && GetCurrentWeek(s.DeadLine).ToString().Equals(week))._id.ToString();
        }
    }
}
