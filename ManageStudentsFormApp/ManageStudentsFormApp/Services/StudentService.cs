﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Repository;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Services
{
    class StudentService:Service
    {
        public StudentService(ICrudRepository<ObjectId, Student> studentRepo, ICrudRepository<ObjectId, Grade> gradeRepo, ICrudRepository<ObjectId, Domain.Task> taskRepo) : base(studentRepo, gradeRepo, taskRepo) { }
        public bool SaveStudent(Student st)
        {
           return _studentRepository.Save(st);   
        }
        public Student GetStudentByName(string name)
        {
            Student student=default(Student);
            GetAll().ToList().ForEach(st => { if (st.Name == name) { student=st; } });
            return student;
        }
        public IList<Student> GetAll()
        {
            return _studentRepository.GetAll().ToList();
        }
    }
}
