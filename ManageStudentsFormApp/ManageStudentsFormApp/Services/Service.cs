﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Repository;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Services
{
    class Service
    {
        protected ICrudRepository<ObjectId, Student> _studentRepository;
        protected ICrudRepository<ObjectId, Grade> _gradeRepository;
        protected ICrudRepository<ObjectId, Domain.Task> _taskRepository;

        public Service(ICrudRepository<ObjectId, Student> studentRepo, ICrudRepository<ObjectId, Grade> gradeRepo,ICrudRepository<ObjectId, Domain.Task> taskRepo)
        {
            _studentRepository = studentRepo;
            _gradeRepository = gradeRepo;
            _taskRepository = taskRepo;
        }
        public Student GetStudent(ObjectId id)
        {
            return _studentRepository.GetElement(id);
        }
        public Domain.Task GetTask(ObjectId id)
        {
            return _taskRepository.GetElement(id);
        }
        public Grade GetGrade(ObjectId id)
        {
            return _gradeRepository.GetElement(id);
        }
    }
}
