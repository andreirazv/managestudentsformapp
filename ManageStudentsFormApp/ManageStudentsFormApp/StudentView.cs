﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManageStudentsFormApp
{
    partial class StudentView : Form
    {
        private StudentService _studentService;

        public StudentView(StudentService studentService)
        {
            _studentService = studentService;
            InitializeComponent();
            InitDataGrid();
            InitDataInGrid();
        }
        private void InitDataGrid()
        {
            DataViewTable.AutoGenerateColumns = false;
            DataViewTable.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Nume", DataPropertyName = "Name", Width = 100 });
            DataViewTable.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Grupa", DataPropertyName = "Group", Width = 100 });
            DataViewTable.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Email", DataPropertyName = "Email", Width = 100 });
            DataViewTable.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Extra Points", DataPropertyName = "ExtraPoints", Width = 100 });

        }
        private void InitDataInGrid()
        {
            List<Student> list = _studentService.GetAll().ToList();

            DataViewTable.DataSource = list.ToArray();

        }
        private void DataViewTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {

        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {

        }

        private void buttonClearText_Click(object sender, EventArgs e)
        {

        }
    }
}
