﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Repository.DataBase
{
    interface IContext<T>
    {

        IMongoCollection<T> Entities { get; set; }
       
    }
}
