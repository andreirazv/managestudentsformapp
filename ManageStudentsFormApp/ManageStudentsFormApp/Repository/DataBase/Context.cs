﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Repository.DataBase
{
    class Context<T> : IContext<T>
    {
         private readonly IMongoDatabase _database;
         private readonly string _collection;
         private MongoClient _client;
         public Context(string collection,MongoClient client)
         {
            _client = client;
            _collection = collection;
             _database = _client.GetDatabase("laboratorMap");
         }
         public IMongoCollection<T> Entities
         {
             get
             {
                 return _database.GetCollection<T>(_collection);
             }
             set => throw new NotImplementedException();
         }
    }
}
