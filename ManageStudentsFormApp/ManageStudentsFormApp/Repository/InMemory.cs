﻿using ManageStudentsFormApp.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Repository
{
    // public delegate E CreateEntity<E>(string line);
    class InMemory<ID, T> : ICrudRepository<ID, T> where T : IHasID<ID>
    {

        //public IValidator<T> _validator;
        public IDictionary<ID, T> _entities = new Dictionary<ID, T>();
        public IDictionary<ID, T> _entitiesDeleted = new Dictionary<ID, T>();
        /*public InMemory(IValidator<T> validator)
        {
            _validator = validator;
        }*/
        public bool Delete(ID key)
        {
            if (key == null)
                throw new ArgumentNullException("key must not be null");
            if (_entities.ContainsKey(key))
            {
                T obj = GetElement(key);
                KeyValuePair<ID, T> item = new KeyValuePair<ID, T>(obj._id, obj);
                _entitiesDeleted.Add(item);

                if (_entities.Remove(key))
                    return true;
            }
            return false;
        }

        public IEnumerable<T> GetAll()
        {
            return _entities.Values.ToList<T>();
        }

        public IEnumerable<T> GetAllDeleted()
        {
            return _entitiesDeleted.Values.ToList<T>();
        }

        public T GetElement(ID key)
        {
            if (key != null)
                return _entities[key];
            else
                throw new FormatException("Acest element nu exista!");
        }

        public bool Save(T obj)
        {
            if (obj == null)
                throw new ArgumentNullException("entity must not be null");
            if (_entities.ContainsKey(obj._id))
                return false;

            KeyValuePair<ID, T> item = new KeyValuePair<ID, T>(obj._id, obj);
            _entities.Add(item);
            return true;
        }

        public bool Update(T obj)
        {
            if (_entities.ContainsKey(obj._id))
            {
                _entities[obj._id] = obj;
                return true;
            }
            return false;
        }
    }

}
