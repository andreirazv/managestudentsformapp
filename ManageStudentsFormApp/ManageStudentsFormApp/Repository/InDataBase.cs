﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Repository.DataBase;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Repository
{
     class InDataBase<ID, T> : ICrudRepository<ID, T> where T: IHasID<ID>
       {
           private readonly IContext<T> _context;
          // private IValidator<T> _validator;
           public InDataBase(string collection,MongoClient client/*, IValidator<T> validator*/)
           {
               _context = new Context<T>(collection,client);
              // _validator = validator;
           }

           public bool Delete(ID key)
           {
               var filter = Builders<T>.Filter.Eq<ID>(g => g._id, key);
               if (_context.Entities.DeleteOne(filter).DeletedCount == 1)
                   return true;
               return false;
           }

           public IEnumerable<T> GetAll()
           {
               var emptyFilter = Builders<T>.Filter.Empty;
               return _context.Entities.FindSync(emptyFilter).ToList();
           }

        public IEnumerable<T> GetAllDeleted()
        {
            throw new NotImplementedException();
        }

        public T GetElement(ID key)
           {
               var filter = Builders<T>.Filter.Eq<ID>(g => g._id, key);
               return _context.Entities.FindSync(filter).FirstOrDefault();
           }

           public bool Save(T obj)
           {
               try
               {
                   _context.Entities.InsertOne(obj);
                   return true;
               }
               catch { 
                   return false;
               }
           }

           public bool Update(T obj)
           {
               var filter = Builders<T>.Filter.Eq(u => u._id, obj._id);
               if(_context.Entities.ReplaceOne(filter, obj).ModifiedCount==1)
                   return true;

               return false;
           }
       }
}
