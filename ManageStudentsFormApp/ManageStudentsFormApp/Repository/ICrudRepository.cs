﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Repository
{
    interface ICrudRepository<ID, T>
    {
        bool Save(T obj);
        T GetElement(ID key);
        IEnumerable<T> GetAll();
        bool Update(T obj);
        bool Delete(ID key);
        IEnumerable<T> GetAllDeleted();
    }
}
