﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Services;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using static ManageStudentsFormApp.Utils.WorkWithDate;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task = ManageStudentsFormApp.Domain.Task;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Threading;
using Font = iTextSharp.text.Font;

namespace ManageStudentsFormApp
{
    partial class GradeView : Form
    {
        private TaskService _taskService;
        private StudentService _studentService;
        private GradeService _gradeService;
        public GradeView(StudentService studentService,TaskService taskService, GradeService gradeService)
        {
            _taskService = taskService;
            _gradeService = gradeService;
            _studentService = studentService;
            InitializeComponent();
            InitDataGrid();
            UpdateData();
            InitCBox();
        }

        private void InitCBox()
        {
            CBoxStudentFiltr.Items.Clear();
            List<string> studentName = _studentService.GetAll().Select(s => s.Name).ToList();
            CBoxStudentFiltr.Text = "";
            if (studentName.Count > 0)
            {
                CBoxStudentFiltr.Items.AddRange(studentName.ToArray());
            }

            CBoxGroupGrFiltr.Items.Clear();
            List<string> groups = new List<string>();
            _studentService.GetAll().ToList().ForEach(s => { if (!groups.Contains(s.Group.ToString())) groups.Add(s.Group.ToString()); });
            CBoxGroupGrFiltr.Text = "";
            if (studentName.Count > 0)
            {
                CBoxGroupGrFiltr.Items.AddRange(groups.ToArray());
            }

            CBoxTaskFiltr.Items.Clear();
            CBoxTaskGrFiltr.Items.Clear();
            List<string> task = new List<string>();
            for(int i=1;i<=14;i++)
                task.Add("Tema " + i);
            CBoxTaskFiltr.Text = "";
            CBoxTaskGrFiltr.Text = "";
            CBoxTaskFiltr.Items.AddRange(task.ToArray());
            CBoxTaskGrFiltr.Items.AddRange(task.ToArray());
            
        }

        private void UpdateData()
        {
            DatePicketTo.Value=(DateTime.Now);
            DatePicketFrom.Value=(DateTime.Now);
            CBoxStudentFiltr.Text = "";
            CBoxGroupGrFiltr.Text = "";
            CBoxTaskFiltr.Text = "";
            CBoxTaskGrFiltr.Text = "";
            List<Grade> list = _gradeService.GetAll().ToList();
            DataViewGrade.DataSource = list.ToArray();
            InitData();
        }
        private void DataViewGrade_CellFormatting(object sender, EventArgs e)
        {
        }
        private void InitData()
        {

            foreach (DataGridViewRow row in DataViewGrade.Rows) {
                Task task = _taskService.GetTask(ObjectId.Parse(row.Cells["TaskId"].Value.ToString()));
                row.Cells["TaskId"].Value = "Tema "+GetCurrentWeek(task.DeadLine);
            }
        }
        private void InitDataGrid()
        {
            DataViewGrade.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataViewGrade.AutoGenerateColumns = false;
            DataViewGrade.Columns.Add(new DataGridViewTextBoxColumn {Name= "Student", HeaderText = "Student", DataPropertyName = "Student",ValueType=typeof(string), Width = 50 });
            DataViewGrade.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Nota", DataPropertyName = "Nota", Width = 50,Name="Nota"});
            DataViewGrade.Columns.Add(new DataGridViewTextBoxColumn { Name = "TaskId", HeaderText = "Task", DataPropertyName = "TaskId", Width = 100 });
            DataViewGrade.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Date", DataPropertyName = "Date", Width = 100 });
            DataViewGrade.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Teacher", DataPropertyName = "Teacher", Width = 100 });
        }
        private Grade CreateGrade()
        {
            ObjectId id = ObjectId.GenerateNewId();
            string teacher = TextBoxTeacher.Text;
            int nota = int.Parse(TextBoxGrade.Text);
            string tema = ComboBoxTask.Text.Split(' ')[1];
            string temaId = _taskService.GetTaskByWeekAndStudent(ComboBoxStudent.Text, tema);
            DateTime dateGrade = DateTime.Now;
            return new Grade { _id = id, Teacher = teacher, Date= dateGrade,Nota=nota,TaskId=temaId };
        }
        private void AddBtnGrade_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_gradeService.SaveGrade(CreateGrade(), CheckBoxMotivat.Checked))
                    MessageBox.Show("Nota adaugata cu succes!");
                else
                    MessageBox.Show("Nota nu a fost adaugata!");
                UpdateData(); InitData();
            }
            catch (Exception)
            {
                MessageBox.Show("Nota nu a fost adaugata!");

            }
        }
        private void GradeView_Load(object sender, EventArgs e)
        {
            ComboBoxStudent.Items.Clear();
            List<Student> studentList = _studentService.GetAll().ToList();
            List<string> nameList = studentList.Select(s => s.Name).ToList();
            ComboBoxStudent.Items.AddRange(nameList.ToArray());
        }
        private void ComboBoxStudent_TextChanged(object sender, EventArgs e)
        {
            if(ComboBoxStudent.Text.Equals(""))
                ComboBoxTask.Items.Clear();
        }
        private void ComboBoxStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxTask.Items.Clear();
            List<Domain.Task> taskList = _taskService.GetAll()
            .Where(s => _studentService.GetStudent(ObjectId.Parse(s.StudentId)).Name.Equals(ComboBoxStudent.Text))
            .ToList();
            List<string> taskDate = taskList.Select(s => "Tema "+GetCurrentWeek(s.DeadLine)).ToList();
            ComboBoxTask.Text = "";
            if (taskDate.Count > 0)
            {
                ComboBoxTask.Items.AddRange(taskDate.ToArray());
                ComboBoxTask.SelectedIndex=0;
            }
        }

        private void ComboBoxTask_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void CBoxTaskFiltr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBoxTaskFiltr != null && !CBoxTaskFiltr.Text.Equals(""))
            {
                DataViewGrade.DataSource = _gradeService.GetByTask(CBoxTaskFiltr.Text).ToArray();
                InitData();
            }
        }

        private void CBoxStudentFiltr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(CBoxStudentFiltr!=null&&!CBoxStudentFiltr.Text.Equals(""))
            {
                DataViewGrade.DataSource = _gradeService.GetByStudent(CBoxStudentFiltr.Text).ToArray();
                InitData();
            }
        }

        private void CBoxGroupGrFiltr_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CBoxTaskGrFiltr_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void DatePicketFrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void DatePicketTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void ButtonListGr_Click(object sender, EventArgs e)
        {
            DataViewGrade.DataSource = _gradeService.GetByGroupAndTask(CBoxGroupGrFiltr.Text,CBoxTaskGrFiltr.Text).ToArray();
            InitData();
        }

        private void ButtonListDate_Click(object sender, EventArgs e)
        {
            DataViewGrade.DataSource =_gradeService.GetByDates(DatePicketFrom.Value,DatePicketTo.Value).ToArray();
            InitData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void BtnStudentsOnTime_Click(object sender, EventArgs e)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Document doc = new Document(PageSize.A4);
                Random random = new Random();
                int rnd = random.Next(100000, 999999);
                string fileName = "Raport" + rnd + ".pdf";
                var output = new FileStream(fileName, FileMode.Create);
                var writer = PdfWriter.GetInstance(doc, output);
                doc.Open();

                Font textFont = FontFactory.GetFont("Arial", 20);
                Paragraph text;
                text = new Paragraph("Studentii care au predat la timp toate temele:", textFont);
                text.Alignment = Element.ALIGN_CENTER;
                doc.Add(text);
                doc.Add(new Chunk("\n"));
                PdfPTable table = new PdfPTable(1);
                table.AddCell(new Phrase("Student", FontFactory.GetFont("Arial", 15)));


                var a = from grade in _gradeService.GetAll()
                        where GetCurrentWeek(_taskService.GetTask(ObjectId.Parse(grade.TaskId)).DeadLine) < GetCurrentWeek(grade.Date)
                        select grade;

                a.ToList().ForEach(x => { Debug.WriteLine(_studentService.GetStudent(ObjectId.Parse(_taskService.GetTask(ObjectId.Parse(x.TaskId)).StudentId)).Name); });
                HashSet<string> vs = new HashSet<string>();
                _gradeService.GetAll().ToList().ForEach(x =>
                {
                    string thisName= _studentService.GetStudent(ObjectId.Parse(_taskService.GetTask(ObjectId.Parse(x.TaskId)).StudentId)).Name;
                    Grade grade = a.ToList().Find(m => _studentService.GetStudent(ObjectId.Parse(_taskService.GetTask(ObjectId.Parse(m.TaskId)).StudentId)).Name.Equals(thisName));
                    if (grade==null)
                    {
                        vs.Add(thisName);
                    }
                });
                vs.ToList().ForEach(x => table.AddCell(x));
                doc.Add(table);
                doc.Close();
                Process.Start(@fileName);
            }).Start();
        }
        class SaveData
        {
            public string Name { get; set; }
            public double Nota { get; set; }
            public double MedieTask { get; set; }
            public double Task { get; set; }
        }
        private void BtnAcceptedStudentInExam_Click(object sender, EventArgs e)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Document doc = new Document(PageSize.A4);
                Random random = new Random();
                int rnd = random.Next(100000, 999999);
                string fileName = "Raport" + rnd + ".pdf";
                var output = new FileStream(fileName, FileMode.Create);
                var writer = PdfWriter.GetInstance(doc, output);
                doc.Open();

                Font textFont = FontFactory.GetFont("Arial", 20);
                Paragraph text;
                text = new Paragraph("Studentii care pot intra in examen:", textFont);
                text.Alignment = Element.ALIGN_CENTER;
                doc.Add(text);
                doc.Add(new Chunk("\n"));
                PdfPTable table = new PdfPTable(2);
                table.AddCell(new Phrase("Student", FontFactory.GetFont("Arial", 15)));
                table.AddCell(new Phrase("Medie", FontFactory.GetFont("Arial", 15)));
            
                var a = from grade in _gradeService.GetAll()
                         group grade by _studentService.GetStudent(ObjectId.Parse(_taskService.GetTask(ObjectId.Parse(grade.TaskId)).StudentId)).Name into g
                         select ( new SaveData {Nota= g.Sum(_ => _.Nota) / 14 ,Name=g.Key}) into m orderby m.Nota descending select m;
                
                a.ToList().ForEach(x =>
                {
                    if (x.Nota >= 4)
                    {
                        table.AddCell(x.Name);
                        table.AddCell(x.Nota.ToString());
                    }
                });
                //_studentService.GetStudent(ObjectId.Parse(_taskService.GetTask(ObjectId.Parse(x.TaskId)).StudentId)).Name.Equals(st.Name)
                doc.Add(table);
                doc.Close();
                Process.Start(@fileName);
            }).Start();
            
        }

        private void BtnHardestTask_Click(object sender, EventArgs e)
        {
  
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Document doc = new Document(PageSize.A4);
                Random random = new Random();
                int rnd = random.Next(100000, 999999);
                string fileName = "Raport" + rnd + ".pdf";
                var output = new FileStream(fileName, FileMode.Create);
                var writer = PdfWriter.GetInstance(doc, output);
                doc.Open();

                Font textFont = FontFactory.GetFont("Arial", 20);
                Paragraph text;
                text = new Paragraph("Tema care are media mai mica:", textFont);
                text.Alignment = Element.ALIGN_CENTER;
                doc.Add(text);
                doc.Add(new Chunk("\n"));
                PdfPTable table = new PdfPTable(2);
                table.AddCell(new Phrase("Tema", FontFactory.GetFont("Arial", 15)));
                table.AddCell(new Phrase("Medie", FontFactory.GetFont("Arial", 15)));


                var a = (from grade in _gradeService.GetAll()
                         group grade by GetCurrentWeek(_taskService.GetTask(ObjectId.Parse(grade.TaskId)).DeadLine) into g
                         select( new SaveData {MedieTask= g.Average(_ => _.Nota),Task=g.Key}) into m orderby m.MedieTask ascending select m).First();

                table.AddCell("Tema "+a.Task);
                table.AddCell(a.MedieTask.ToString());

                doc.Add(table);
                doc.Close();
                Process.Start(@fileName);
            }).Start();
        }

        private void BtnTaskGradeEachStudent_Click(object sender, EventArgs e)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Document doc = new Document(PageSize.A4.Rotate());
                Random random = new Random();
                int rnd = random.Next(100000, 999999);
                string fileName = "Raport" + rnd + ".pdf";
                var output = new FileStream(fileName, FileMode.Create);
                var writer = PdfWriter.GetInstance(doc, output);
                doc.Open();

                Font textFont = FontFactory.GetFont("Arial", 20);
                Paragraph text;
                text = new Paragraph("Notele pentru laborator la fiecare student:", textFont);
                text.Alignment = Element.ALIGN_CENTER;
                doc.Add(text);
                doc.Add(new Chunk("\n"));
                PdfPTable table = new PdfPTable(16);
                table.AddCell(new Phrase("Student", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema1", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema2", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema3", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema4", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema5", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema6", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema7", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema8", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema9", FontFactory.GetFont("Arial", 10)));
                table.AddCell(new Phrase("Tema10", FontFactory.GetFont("Arial", 9)));
                table.AddCell(new Phrase("Tema11", FontFactory.GetFont("Arial", 9)));
                table.AddCell(new Phrase("Tema12", FontFactory.GetFont("Arial", 9)));
                table.AddCell(new Phrase("Tema13", FontFactory.GetFont("Arial", 9)));
                table.AddCell(new Phrase("Tema14", FontFactory.GetFont("Arial", 9)));
                table.AddCell(new Phrase("Medie", FontFactory.GetFont("Arial", 10)));
                _studentService.GetAll().ToList().ForEach(st =>
                {
                    double medie = default(double);
                    table.AddCell(new Phrase(st.Name, FontFactory.GetFont("Arial", 10)));
                    for (int i = 1; i <= 14; i++)
                    {
                        Grade grade = (from gr in _gradeService.GetAll()
                                      where _studentService.GetStudent(ObjectId.Parse(_taskService.GetTask(ObjectId.Parse(gr.TaskId)).StudentId)).Name.Equals(st.Name) && GetCurrentWeek(_taskService.GetTask(ObjectId.Parse(gr.TaskId)).DeadLine).ToString().Equals(@i.ToString())
                                      select gr).FirstOrDefault();

                        if (grade != null)
                        {
                            medie += grade.Nota;
                            table.AddCell(grade.Nota.ToString());
                        }
                        else
                        {
                            table.AddCell("1");
                            medie += 1;
                        }
                    }
                    table.AddCell(Math.Round(medie/14, 2).ToString());
                });

                doc.Add(table);
                doc.Close();
                Process.Start(@fileName);
            }).Start();
        }

        private void DataViewGrade_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
