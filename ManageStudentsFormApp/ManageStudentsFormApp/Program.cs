﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Repository;
using ManageStudentsFormApp.Services;
using MongoDB.Bson;
using MongoDB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task = ManageStudentsFormApp.Domain.Task;
using MongoDB.Driver;
using System.Configuration;

namespace ManageStudentsFormApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            MongoClient client = new MongoClient();
            ICrudRepository<ObjectId, Student> studentRepo = new InDataBase<ObjectId, Student>("studenti", client);
            ICrudRepository<ObjectId, Grade> gradeRepo = new InDataBase<ObjectId, Grade>("catalog", client);
            ICrudRepository<ObjectId, Domain.Task> taskRepo = new InDataBase<ObjectId, Domain.Task>("task", client);
            StudentService studentService = new StudentService(studentRepo, gradeRepo, taskRepo);
            TaskService taskService = new TaskService(studentRepo, gradeRepo, taskRepo);
            GradeService gradeService = new GradeService(studentRepo, gradeRepo, taskRepo);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainMenu(studentService,taskService,gradeService));
        }
    }
}
