﻿namespace ManageStudentsFormApp
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StudentBtnStart = new System.Windows.Forms.Button();
            this.TaskBtnStart = new System.Windows.Forms.Button();
            this.GradeBtnStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // StudentBtnStart
            // 
            this.StudentBtnStart.Location = new System.Drawing.Point(113, 162);
            this.StudentBtnStart.Name = "StudentBtnStart";
            this.StudentBtnStart.Size = new System.Drawing.Size(124, 127);
            this.StudentBtnStart.TabIndex = 0;
            this.StudentBtnStart.Text = "Student";
            this.StudentBtnStart.UseVisualStyleBackColor = true;
            this.StudentBtnStart.Click += new System.EventHandler(this.StudentBtnStart_Click);
            // 
            // TaskBtnStart
            // 
            this.TaskBtnStart.Location = new System.Drawing.Point(301, 162);
            this.TaskBtnStart.Name = "TaskBtnStart";
            this.TaskBtnStart.Size = new System.Drawing.Size(124, 127);
            this.TaskBtnStart.TabIndex = 1;
            this.TaskBtnStart.Text = "Task";
            this.TaskBtnStart.UseVisualStyleBackColor = true;
            this.TaskBtnStart.Click += new System.EventHandler(this.TaskBtnStart_Click);
            // 
            // GradeBtnStart
            // 
            this.GradeBtnStart.Location = new System.Drawing.Point(492, 162);
            this.GradeBtnStart.Name = "GradeBtnStart";
            this.GradeBtnStart.Size = new System.Drawing.Size(124, 127);
            this.GradeBtnStart.TabIndex = 2;
            this.GradeBtnStart.Text = "Grade";
            this.GradeBtnStart.UseVisualStyleBackColor = true;
            this.GradeBtnStart.Click += new System.EventHandler(this.GradeBtnStart_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GradeBtnStart);
            this.Controls.Add(this.TaskBtnStart);
            this.Controls.Add(this.StudentBtnStart);
            this.Name = "MainMenu";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StudentBtnStart;
        private System.Windows.Forms.Button TaskBtnStart;
        private System.Windows.Forms.Button GradeBtnStart;
    }
}

