﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Domain
{
    class Student: IHasID<ObjectId>
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string Name { get; set; }
        public int Group { get; set; }
        public string Email { get; set; }
        public double ExtraPoints { get; set; }
        public bool Idle { get; set; }
    }
}
