﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Domain
{
    interface IHasID<ID>
    {
        [BsonIgnoreIfDefault]
        ID _id { get; set; }
    }
}
