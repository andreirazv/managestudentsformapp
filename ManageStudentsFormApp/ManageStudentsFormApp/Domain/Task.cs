﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Domain
{
    class Task : IHasID<ObjectId>
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string Description { get; set; }
        public DateTime DeadLine { get; set; }
        public string StudentId { get; set; }
        public string Teacher { get; set; }
    }
}
