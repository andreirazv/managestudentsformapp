﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudentsFormApp.Domain
{
    class Grade: IHasID<ObjectId>
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public double Nota { get; set; }
        public string TaskId { get; set; }
        public string Teacher { get; set; }
        public DateTime Date { get; set; }
    }
}
