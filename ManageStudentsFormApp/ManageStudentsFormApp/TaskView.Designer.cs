﻿namespace ManageStudentsFormApp
{
    partial class TaskView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataViewTask = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TextBoxDescriere = new System.Windows.Forms.TextBox();
            this.TextBoxTeacher = new System.Windows.Forms.TextBox();
            this.DatePickerDeadLine = new System.Windows.Forms.DateTimePicker();
            this.ComboBoxStudent = new System.Windows.Forms.ComboBox();
            this.AddBtnTask = new System.Windows.Forms.Button();
            this.ClearBtnTask = new System.Windows.Forms.Button();
            this.ExtendBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataViewTask)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataViewTask
            // 
            this.DataViewTask.AllowUserToAddRows = false;
            this.DataViewTask.AllowUserToDeleteRows = false;
            this.DataViewTask.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataViewTask.Location = new System.Drawing.Point(12, 12);
            this.DataViewTask.Name = "DataViewTask";
            this.DataViewTask.ReadOnly = true;
            this.DataViewTask.RowTemplate.Height = 24;
            this.DataViewTask.Size = new System.Drawing.Size(608, 426);
            this.DataViewTask.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(712, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Task Information";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.93734F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.06266F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.TextBoxDescriere, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TextBoxTeacher, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.DatePickerDeadLine, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ComboBoxStudent, 1, 2);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(663, 84);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(433, 220);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Descriere";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Teacher";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Student";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 25);
            this.label5.TabIndex = 3;
            this.label5.Text = "DeadLine";
            // 
            // TextBoxDescriere
            // 
            this.TextBoxDescriere.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescriere.Location = new System.Drawing.Point(123, 3);
            this.TextBoxDescriere.Multiline = true;
            this.TextBoxDescriere.Name = "TextBoxDescriere";
            this.TextBoxDescriere.Size = new System.Drawing.Size(140, 49);
            this.TextBoxDescriere.TabIndex = 4;
            // 
            // TextBoxTeacher
            // 
            this.TextBoxTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxTeacher.Location = new System.Drawing.Point(123, 58);
            this.TextBoxTeacher.Name = "TextBoxTeacher";
            this.TextBoxTeacher.Size = new System.Drawing.Size(140, 30);
            this.TextBoxTeacher.TabIndex = 5;
            this.TextBoxTeacher.TextChanged += new System.EventHandler(this.TextBoxTeacher_TextChanged);
            // 
            // DatePickerDeadLine
            // 
            this.DatePickerDeadLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerDeadLine.Location = new System.Drawing.Point(123, 168);
            this.DatePickerDeadLine.Name = "DatePickerDeadLine";
            this.DatePickerDeadLine.Size = new System.Drawing.Size(260, 23);
            this.DatePickerDeadLine.TabIndex = 10;
            this.DatePickerDeadLine.ValueChanged += new System.EventHandler(this.DatePickerDeadLine_ValueChanged);
            // 
            // ComboBoxStudent
            // 
            this.ComboBoxStudent.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComboBoxStudent.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ComboBoxStudent.FormattingEnabled = true;
            this.ComboBoxStudent.Location = new System.Drawing.Point(123, 113);
            this.ComboBoxStudent.Name = "ComboBoxStudent";
            this.ComboBoxStudent.Size = new System.Drawing.Size(140, 33);
            this.ComboBoxStudent.TabIndex = 11;
            this.ComboBoxStudent.SelectedIndexChanged += new System.EventHandler(this.ComboBoxStudent_SelectedIndexChanged);
            // 
            // AddBtnTask
            // 
            this.AddBtnTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtnTask.Location = new System.Drawing.Point(663, 367);
            this.AddBtnTask.Name = "AddBtnTask";
            this.AddBtnTask.Size = new System.Drawing.Size(75, 36);
            this.AddBtnTask.TabIndex = 8;
            this.AddBtnTask.Text = "Add";
            this.AddBtnTask.UseVisualStyleBackColor = true;
            this.AddBtnTask.Click += new System.EventHandler(this.AddBtnTask_Click);
            // 
            // ClearBtnTask
            // 
            this.ClearBtnTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearBtnTask.Location = new System.Drawing.Point(952, 367);
            this.ClearBtnTask.Name = "ClearBtnTask";
            this.ClearBtnTask.Size = new System.Drawing.Size(75, 36);
            this.ClearBtnTask.TabIndex = 9;
            this.ClearBtnTask.Text = "Clear";
            this.ClearBtnTask.UseVisualStyleBackColor = true;
            this.ClearBtnTask.Click += new System.EventHandler(this.ClearBtnTask_Click);
            // 
            // ExtendBtn
            // 
            this.ExtendBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtendBtn.Location = new System.Drawing.Point(754, 367);
            this.ExtendBtn.Name = "ExtendBtn";
            this.ExtendBtn.Size = new System.Drawing.Size(186, 36);
            this.ExtendBtn.TabIndex = 3;
            this.ExtendBtn.Text = "Extend Dead Line";
            this.ExtendBtn.UseVisualStyleBackColor = true;
            this.ExtendBtn.Click += new System.EventHandler(this.ExtendBtn_Click);
            // 
            // TaskView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1105, 473);
            this.Controls.Add(this.ExtendBtn);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DataViewTask);
            this.Controls.Add(this.ClearBtnTask);
            this.Controls.Add(this.AddBtnTask);
            this.Name = "TaskView";
            this.Text = "TaskView";
            this.Load += new System.EventHandler(this.TaskView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataViewTask)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataViewTask;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextBoxDescriere;
        private System.Windows.Forms.TextBox TextBoxTeacher;
        private System.Windows.Forms.Button AddBtnTask;
        private System.Windows.Forms.Button ClearBtnTask;
        private System.Windows.Forms.DateTimePicker DatePickerDeadLine;
        private System.Windows.Forms.Button ExtendBtn;
        private System.Windows.Forms.ComboBox ComboBoxStudent;
    }
}