﻿using ManageStudentsFormApp.Domain;
using ManageStudentsFormApp.Services;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ManageStudentsFormApp.Utils.WorkWithDate;
using System.Windows.Forms;

namespace ManageStudentsFormApp
{
    partial class TaskView : Form
    {
        private TaskService _taskService;
        private StudentService _studentService;
       // AutoCompleteStringCollection collection = new AutoCompleteStringCollection();

        public TaskView(TaskService taskService, StudentService studentService)
        {
            _taskService = taskService;
            _studentService = studentService;
            InitializeComponent();
            InitDataGrid();
            UpdateData();
            InitNameData();
        }

        private void InitNameData()
        {
            foreach (DataGridViewRow row in DataViewTask.Rows)
            {
                Student st = _taskService.GetStudent(ObjectId.Parse(row.Cells["StudentId"].Value.ToString()));
                row.Cells["StudentId"].Value = st.Name.ToString();
            }
        }

        private void InitDataGrid()
        {
            DataViewTask.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataViewTask.AutoGenerateColumns = false;
            DataViewTask.Columns.Add(new DataGridViewTextBoxColumn { Name= "Description", HeaderText = "Descriere", DataPropertyName = "Description", Width = 100 });
            DataViewTask.Columns.Add(new DataGridViewTextBoxColumn { Name = "StudentId", HeaderText = "Student", DataPropertyName = "StudentId", Width = 100 });
            DataViewTask.Columns.Add(new DataGridViewTextBoxColumn { Name = "DeadLine", HeaderText = "Dead Line", DataPropertyName = "DeadLine", Width = 100 });
            DataViewTask.Columns.Add(new DataGridViewTextBoxColumn { Name = "Teacher", HeaderText = "Teacher", DataPropertyName = "Teacher", Width = 100 });
        }
        private void UpdateData()
        {
            List<Domain.Task> list = _taskService.GetAll().ToList();

            DataViewTask.DataSource = list.ToArray();

        }

        private void AddBtnTask_Click(object sender, EventArgs e)
        {
            if (_taskService.SaveTask(CreateTask()) == true)
                 MessageBox.Show("Task adaugat cu succes!");
             else
                 MessageBox.Show("Taskul nu a fost adaugat!");
             UpdateData(); InitNameData();
        }
        private Domain.Task CreateTask()
        {
            ObjectId id = ObjectId.GenerateNewId();
            string teacher = TextBoxTeacher.Text;
            string description = TextBoxDescriere.Text;
            ObjectId studenId = _studentService.GetStudentByName(ComboBoxStudent.Text)._id;
            DateTime deadLine = DatePickerDeadLine.Value.Date;
            return new Domain.Task {_id= id,Teacher=teacher,Description=description,StudentId=studenId.ToString(),DeadLine=deadLine };
        }

        private void ClearBtnTask_Click(object sender, EventArgs e)
        {
            TextBoxDescriere.Clear();
            TextBoxTeacher.Clear();
            DatePickerDeadLine.Value = DateTime.Now;
        }

        private void ExtendBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataViewTask.SelectedRows.Count == 1)
                {

                    //int id = 
                    string id = "";
                    foreach (DataGridViewRow row in DataViewTask.SelectedRows)
                    {
                        id = row.Cells[1].Value.ToString();
                    }

                    ObjectId idTask = _studentService.GetStudent(ObjectId.Parse(id))._id;
                    _taskService.ExtendDeadline(idTask, 1);

                }
                else
                    MessageBox.Show("Trebuie sa fie o tema selectata!");
                UpdateData(); InitNameData();
                // _taskService.ExtendDeadline(id, 1);
            }
            catch ( Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void TextBoxTeacher_TextChanged(object sender, EventArgs e)
        {

        }

       /* private void ComboBoxStudent_TextChanged(object sender, EventArgs e)
        {
            ComboBox t = sender as ComboBox;
            if (t != null)
            {
                if (t.Text.Length > 0)
                {
                    ComboBoxStudent.Items.Clear();
                    List<Student> studentList = _studentService.GetAll().ToList();
                    List<string> nameList = studentList.Select(s => s.Name).ToList();
                    string[] filterList = nameList.Where(s => s.StartsWith(t.Text)).ToArray();
                    ComboBoxStudent.Items.AddRange(filterList);
                    //ComboBoxStudent.AutoCompleteCustomSource = collection;
                }
            }
        }*/

        private void TaskView_Load(object sender, EventArgs e)
        {
            ComboBoxStudent.Items.Clear();
            List<Student> studentList = _studentService.GetAll().ToList();
            List<string> nameList = studentList.Select(s => s.Name).ToList();
            ComboBoxStudent.Items.AddRange(nameList.ToArray());
        }

        private void ComboBoxStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void DatePickerDeadLine_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                GetCurrentWeek(DatePickerDeadLine.Value);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                DatePickerDeadLine.Value = DateTime.Now;
            }
        }
    }
}
